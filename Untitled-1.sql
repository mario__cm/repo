CREATE DATABASE colegio;

USE colegio;

CREATE TABLE persona(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	dni CHAR(9) NOT NULL UNIQUE,
	nombre VARCHAR(50) NOT NULL,
	apellido VARCHAR(50) NOT NULL,
	telefono CHAR(9)
);

CREATE TABLE alumno(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    id_persona INT UNSIGNED REFERENCES persona,
    n_expediente INT UNSIGNED UNIQUE NOT NULL,
    id_alumno INT UNSIGNED REFERENCES alumno
);

CREATE TABLE profesor(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    id_persona INT UNSIGNED REFERENCES persona,
    email VARCHAR(50)
);

CREATE TABLE asignatura(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    horas INT
);

CREATE TABLE curso(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    codigo CHAR(3) NOT NULL UNIQUE,
    horario VARCHAR(200)
    aula CHAR(3) NOT NULL,
    id_profesor INT UNSIGNED REFERENCES profesor
    );
CREATE TABLE alumno_asignatura(
    id_alumno INT UNSIGNED REFERENCES alumno,
    id_asignatura INT UNSIGNED REFERENCES asignatura,
    notas FLOAT UNSIGNED,
    PRIMARY KEY(id_alumno, id_asignatura)
);

CREATE TABLE profesor_asignatura(
    id_profesor INT UNSIGNED REFERENCES profesor,
    id_asignatura INT UNSIGNED REFERENCES,
    PRIMARY KEY(id_profesor,id_asignatura)
);

create table asignatura_curso(
	id_asignatura int unsigned references asignatura,
	id_curso int unsigned references curso,
	primary key(id_asignatura,id_curso)
);